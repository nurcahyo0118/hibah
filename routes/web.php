<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });







// Auth::routes();




Route::get('/timeline', function () {
    return view('comment');
});

// Route::get('dashboard', 'ProfileController@dashboard');



Route::get('noPermission', 'DashboardController@noPermission')->name('noPermission');

Route::post('register', 'Auth\RegisterController@register');

Route::get('register', 'HomeController@register')->name('register');

Route::get( '/', [
  'as' => '/',
  'uses' => 'LoginController@getLogin'
]);


Route::post( 'login', [
  'as' => 'login',
  'uses' => 'LoginController@postLogin'
]);

Route::group(['middleware' => ['authen']], function(){
  Route::post( '/logout', [
    'as' => 'logout',
    'uses' => 'LoginController@getLogout'
  ]);



  Route::post('comments/post/{id}', 'CommentController@store');


  Route::get('/home', 'HomeController@index')->name('home');

  Route::get('me', 'ProfileController@index')->name('me');
  Route::get('me/edit', 'ProfileController@edit');
  Route::put('me/update', 'ProfileController@update')->name('me.update');

  Route::post('me/update-image', 'ProfileController@changeImage')->name('changeImage');

  Route::post('me/update-password', 'ProfileController@changePassword')->name('changePassword');

  Route::get('/dashboard', ['as'=>'dashboard', 'uses'=>'DashboardController@dashboard']);
});

Route::group(['middleware' => ['authen', 'roles'],'roles' => 'reviewer'], function(){
  Route::get('/assessment', function(){
    return view('assessments.index');
  });

  Route::get('assessment/{id}', 'AssessmentController@show');
  Route::put('assessment/{id}', 'AssessmentController@store')->name('assessment');
  Route::get('assessments', 'AssessmentController@index')->name('assessments.index');
});

Route::group(['middleware' => ['authen', 'roles'],'roles' => 'pendamping'], function(){
  Route::get('/monevs', 'MonevController@monev');
  Route::put('approve-monev/{id}', 'MonevController@approveMonev')->name('approveMonev');
  Route::put('unapprove-monev/{id}', 'MonevController@unapproveMonev')->name('unapproveMonev');
  Route::put('approve-research/{id}', 'ResearchController@approveResearch')->name('approveResearch');
  Route::put('unapprove-research/{id}', 'ResearchController@unapproveResearch')->name('unapproveResearch');
  Route::get('advisor/researches', 'ResearchController@advisor')->name('advisor.researches');
  Route::get('advisor/research/seemember/{id}', 'ResearchController@advisorSeeMember')->name('advisor.research.seemember');
  Route::get('advisor/research/{id}/reports', 'MonevController@advisorindex')->name('advisor.research.reports');
  Route::get('advisor/research/{id}', 'ResearchController@advisordetail')->name('advisor.research.detail');
});


Route::group(['middleware' => ['authen', 'roles'],'roles' => 'peneliti'], function(){
  Route::get('research/{id}/reports', 'MonevController@index')->name('research.reports');
  Route::post('research-report/{id}', 'MonevController@store')->name('research.report');
  Route::get('research', 'ResearchController@me');
  Route::get('research/addmember/{id}', 'ResearchController@addmember')->name('research.addmember');
  Route::post('research/addingmember/{id}', 'ResearchController@addingmember')->name('research.addingmember');
  Route::post('research/removemember/{id}', 'ResearchController@removemember')->name('research.removemember');
  Route::get('research/{id}', 'ResearchController@detail')->name('research.detail');
  Route::post('research', 'ResearchController@store')->name('research.store');
  Route::put('research/{id}', 'ResearchController@update')->name('research.update');
  Route::get('research/show/{id}', 'ResearchController@show')->name('research.show');
  Route::get('all', 'ResearchController@all');
});

Route::group(['middleware' => ['authen', 'roles'],'roles' => 'operator'], function(){
  Route::resource('researches', 'ResearchController');
  Route::put('update-reviewer/{id}', 'ResearchController@selectReviewer')->name('selectReviewer');
  Route::get('operator/research/{id}', 'ResearchController@advisordetail')->name('operator.research.detail');
});

Route::group(['middleware' => ['authen', 'roles'],'roles' => 'administrator'], function(){
  Route::get('/roles', 'RoleController@index');
  Route::put('user/update-role/{id}', 'UserController@changeRole')->name('changeRole');
  Route::get('administrator/researches', 'ResearchController@administrator')->name('administrator.researches');
  Route::resource('users', 'UserController');
  Route::get('administrator/research/{id}', 'ResearchController@administratordetail')->name('administrator.research.detail');
});
