<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  public function research(){
    $this->belongsTo('App\Research', 'research_id');
  }

}
