<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
  use AuthenticatesUsers;

  protected $email = 'email';
  // protected $redirectTo = '/researches';
  protected $guard = 'guest';



  public function getLogin(){
    if (Auth::guard('web')->check()) {
      return redirect()->route('me');
    }

    return view('login');
  }

  public function postLogin(Request $request){
    $auth = Auth::guard('web')->attempt([
      'email' => $request->email,
      'password' => $request->password
    ]);

    if ($auth) {
      return redirect()->route('researches.index');
    }

    return redirect()->route('/');
  }


  public function getLogout(){
    Auth::guard('web')->logout();
    return redirect()->route('/');
  }
}
