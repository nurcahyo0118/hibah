<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
  public function assessment(){
    return $this->hasOne('App\Assessment', 'research_id');
  }

  // Leader
  public function leader(){
    return $this->belongsTo('App\User', 'author_id');
  }

  public function comments(){
      return $this->hasMany('App\Comment', 'author_id');
  }

  public function reviewer(){
    return $this->hasOne('App\User', 'id', 'reviewer_id');
  }

  public function members(){
    return $this->belongsToMany('App\User');
  }

  public function monevs(){
    return $this->hasMany('App\Monev', 'research_id');
  }

}
