@extends('layouts.main')

@section('content')

<div class="row">




<div class="col-xs-12">


  <div class="box box-danger">

    <div class="box-header">
      <!-- <h3 class="box-title">Daftar tag</h3> -->
      <!-- <div class="col-xs-12">
        <a class="btn btn-info pull-left col-md-1" href="#!" data-toggle="modal" data-target="#modalAdd">
          <span class="fa fa-plus"></span>
        </a>

      </div> -->

    </div>

    <div class="box-body">
      <table class="table js-basic-example dataTable">
        <thead>
            <tr>
                <th>Nama</th>
                <th>E-Mail</th>
                <th>Peran</th>
                <th>##</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)

        <div class="modal fade" id="modalUser{{ $user->id }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">{{ $user->name }}</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-6">
                            <label>Nama Lengkap :</label>
                            <p>{{ $user->name }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>E-mail :</label>
                            <p>{{ $user->email }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>NIP :</label>
                            <p>{{ $user->nip }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Tempat, Tanggal Lahir :</label>
                            <p>{{ $user->pob }}, {{ date('d F Y', strtotime($user->dob)) }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Jenis Kelamin :</label>
                            <p>{{ $user->gender }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Status :</label>
                            <p>{{ $user->married }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Agama :</label>
                            <p>{{ $user->religion }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Golongan :</label>
                            <p>{{ $user->golongan }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>PT :</label>
                            <p>{{ $user->pt }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Alamat PT :</label>
                            <p>{{ $user->pt_address }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Telepon PT :</label>
                            <p>{{ $user->pt_telephone }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Alamat :</label>
                            <p>{{ $user->address }}</p>
                          </div>
                          <div class="col-md-6">
                            <label>Telepon :</label>
                            <p>{{ $user->telephone }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>

            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                  {!! Form::open(['route' => ['changeRole', $user->id], 'method' => 'PUT', 'data-parsley-validate' => '', 'files' => true]) !!}
                    <fieldset>

                      <div class="col-md-12">
                        <div class="form-group">
                          <select class="form-control" name="role_id">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ $user->role->id === $role->id ? 'selected' : ''}}>{{ $role->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <button type="submit" class="btn btn-info"><i class="fa fa-refresh"></i></button>
                      </div>

                    </fieldset>
                  {!! Form::close() !!}
                </td>

                <td>
                  <a href="#!" data-toggle="modal" data-target="#modalUser{{ $user->id }}" class="btn btn-info btn-xs"><i class="fa fa-list"></i></a>
                  <a href="#!" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
      </table>

    </div>

  </div>

</div>
</div>

@endsection
