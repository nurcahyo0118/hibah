<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    <!-- <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>

      <!-- <li>
        <a href="{{ url('dashboard') }}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li> -->

      @if( Auth::user()->role->name == 'peneliti' )

      <li>
        <a href="{{ url('research') }}">
          <i class="fa fa-dashboard"></i> <span>Penelitian</span>
        </a>
      </li>
      <li>
        <a href="{{ url('all') }}">
          <i class="fa fa-list"></i> <span>Penelitian</span>
        </a>
      </li>

      @elseif( Auth::user()->role->name == 'operator' )

      <li>
        <a href="{{ route('researches.index') }}">
          <i class="fa fa-search"></i> <span>Penelitian</span>
        </a>
      </li>

      @elseif( Auth::user()->role->name == 'reviewer' )

      <li>
        <a href="{{ route('assessments.index') }}">
          <i class="fa fa-sort-numeric-asc"></i> <span>Penilaian</span>
        </a>
      </li>

      @elseif( Auth::user()->role->name == 'pendamping' )

      <li class="treeview active">
        <a href="#">
          <i class="fa fa-cubes"></i>
          <span>Menu</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('advisor.researches') }}"><i class="fa fa-circle-o"></i> Penelitian</a></li>
          <li><a href="{{ url('monevs') }}"><i class="fa fa-circle-o"></i> Laporan Monev</a></li>
        </ul>
      </li>

      @elseif( Auth::user()->role->name == 'administrator' )

      <li class="treeview active">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Manajemen Pengguna</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i> Pengguna</a></li>
          <li><a href="{{ url('roles') }}"><i class="fa fa-circle-o"></i> Peran</a></li>
          <li><a href="{{ route('administrator.researches') }}"><i class="fa fa-circle-o"></i> Penelitian</a></li>
        </ul>
      </li>

      @endif




      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Charts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
        </ul>
      </li> -->



      <!-- <li><a href="documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      <li class="header">LABELS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
