<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('nip')->nullable();
            $table->string('pob')->nullable();
            $table->date('dob')->nullable();
            $table->enum('gender', ['L', 'P'])->nullable();
            $table->enum('married', ['MENIKAH', 'BELUM'])->nullable();
            $table->string('religion')->nullable();
            $table->string('golongan')->nullable();
            $table->string('pt')->nullable();
            $table->string('pt_address')->nullable();
            $table->string('pt_telephone')->nullable();
            $table->string('address')->nullable();
            $table->string('telephone')->nullable();
            $table->string('image')->nullable();
            $table->string('role_id')->foreign('role_id')->references('id')->on('roles')->default(2);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
