<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('researches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('file');
            $table->bigInteger('cost');
            $table->date('date');
            $table->text('abstract');
            $table->integer('author_id')->foreign('author_id')->references('id')->on('users')->nullable();
            $table->integer('reviewer_id')->foreign('reviewer_id')->references('id')->on('users')->nullable();
            $table->boolean('granted')->default(false);
            $table->boolean('approved')->default(false);
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('researches');
    }
}
