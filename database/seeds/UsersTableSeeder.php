<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $operator = new User();
      $operator->name = 'operator';
      $operator->email = 'operator@hibah.com';
      $operator->nip = '11111';
      $operator->pob = 'Indonesia';
      $operator->dob = '2000-04-07';
      $operator->gender = 'L';
      $operator->married = 'BELUM';
      $operator->religion = 'Islam';
      $operator->golongan = 'Lektor Kepala';
      $operator->pt = 'UMS';
      $operator->pt_address = 'Surakarta';
      $operator->pt_telephone = '(0294)643155';
      $operator->address = 'Surakarta';
      $operator->telephone = '081225138574';
      $operator->password = bcrypt('operator');
      $operator->role_id = 1;
      // $operator->research_id = 1;
      $operator->save();

      $peneliti = new User();
      $peneliti->name = 'Peneliti';
      $peneliti->email = 'peneliti@hibah.com';
      $peneliti->nip = '11111';
      $peneliti->pob = 'Indonesia';
      $peneliti->dob = '2000-04-07';
      $peneliti->gender = 'L';
      $peneliti->married = 'BELUM';
      $peneliti->religion = 'Islam';
      $peneliti->golongan = 'Lektor Kepala';
      $peneliti->pt = 'UMS';
      $peneliti->pt_address = 'Surakarta';
      $peneliti->pt_telephone = '(0294)643155';
      $peneliti->address = 'Surakarta';
      $peneliti->telephone = '081225138574';
      $peneliti->password = bcrypt('peneliti');
      $peneliti->role_id = 2;
      // $operator->research_id = 4;
      $peneliti->save();

      $reviewer = new User();
      $reviewer->name = 'Reviewer';
      $reviewer->email = 'reviewer@hibah.com';
      $reviewer->nip = '11111';
      $reviewer->pob = 'Indonesia';
      $reviewer->dob = '2000-04-07';
      $reviewer->gender = 'L';
      $reviewer->married = 'BELUM';
      $reviewer->religion = 'Islam';
      $reviewer->golongan = 'Lektor Kepala';
      $reviewer->pt = 'UMS';
      $reviewer->pt_address = 'Surakarta';
      $reviewer->pt_telephone = '(0294)643155';
      $reviewer->address = 'Surakarta';
      $reviewer->telephone = '081225138574';
      $reviewer->password = bcrypt('reviewer');
      $reviewer->role_id = 3;
      // $operator->research_id = 4;
      $reviewer->save();

      $pembimbing = new User();
      $pembimbing->name = 'Pembimbing';
      $pembimbing->email = 'pembimbing@hibah.com';
      $pembimbing->nip = '11111';
      $pembimbing->pob = 'Indonesia';
      $pembimbing->dob = '2000-04-07';
      $pembimbing->gender = 'L';
      $pembimbing->married = 'BELUM';
      $pembimbing->religion = 'Islam';
      $pembimbing->golongan = 'Lektor Kepala';
      $pembimbing->pt = 'UMS';
      $pembimbing->pt_address = 'Surakarta';
      $pembimbing->pt_telephone = '(0294)643155';
      $pembimbing->address = 'Surakarta';
      $pembimbing->telephone = '081225138574';
      $pembimbing->password = bcrypt('pembimbing');
      $pembimbing->role_id = 4;
      // $operator->research_id = 4;
      $pembimbing->save();

      $pembimbing = new User();
      $pembimbing->name = 'administrator';
      $pembimbing->email = 'administrator@hibah.com';
      $pembimbing->nip = '11111';
      $pembimbing->pob = 'Indonesia';
      $pembimbing->dob = '2000-04-07';
      $pembimbing->gender = 'L';
      $pembimbing->married = 'BELUM';
      $pembimbing->religion = 'Islam';
      $pembimbing->golongan = 'Lektor Kepala';
      $pembimbing->pt = 'UMS';
      $pembimbing->pt_address = 'Surakarta';
      $pembimbing->pt_telephone = '(0294)643155';
      $pembimbing->address = 'Surakarta';
      $pembimbing->telephone = '081225138574';
      $pembimbing->password = bcrypt('administrator');
      $pembimbing->role_id = 5;
      // $operator->research_id = 4;
      $pembimbing->save();

    }
}
