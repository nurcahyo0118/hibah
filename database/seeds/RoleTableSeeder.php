<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_operator = new Role();
        $role_operator->name = 'operator';
        $role_operator->description = '';
        $role_operator->save();

        $role_peneliti = new Role();
        $role_peneliti->name = 'peneliti';
        $role_peneliti->description = '';
        $role_peneliti->save();

        $role_reviewer = new Role();
        $role_reviewer->name = 'reviewer';
        $role_reviewer->description = '';
        $role_reviewer->save();

        $role_pembimbing = new Role();
        $role_pembimbing->name = 'pendamping';
        $role_pembimbing->description = '';
        $role_pembimbing->save();

        $role_pembimbing = new Role();
        $role_pembimbing->name = 'administrator';
        $role_pembimbing->description = '';
        $role_pembimbing->save();
    }
}
